<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">  	
    	<meta name="viewport" content="width=device-width,user-scalable=no"/>
		<title>Friends list</title>
		<link rel="stylesheet"  href="./lib/tau/mobile/theme/default/tau.css">
		<link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="./css/style.css">
		<link rel="stylesheet" type="text/css" href="./css/substyle.css">
		
	</head>

	<body>
		<div class="ui-page" id="pageFriends" style="overflow: auto;">
			<div class="main">
				<div class="submain">	
				
					<div class="subheader">
						<a href="menu.html" title="Back to the Home page"><b>HOME</b></a>|
						<a href="schedule.html" title="To the schedule"><b>BACK TO SCHEDULE</b></a>
						<br>
						<h1 id="subh1"><span>My friends</span></h1>
					</div>
					
					<form action="friendslist.php" method="post">
						<input type="text" name="searchlist" id="FriendsSearchlist" placeholder="Search a friend...">
						<span id="FriendsSuggestion"></span>
						<br>
					
						<a href=""><button type="submit" value="Submit" name="confirm" id="addfriends">Add More Friends</button></a>
					</form>
					<!-- <h4 style="text-align:left;">You have 9 friends:</h4> -->
					
					<div id="FriendsSublist">
						<?php
						
						session_start();
						require 'requires/connect.inc.php';
						
						if(isset($_POST['confirm']))
						{
							$username=htmlentities($_POST["searchlist"]);
							
							require 'requires/connect.inc.php';
							
							$sql = "SELECT userID FROM Users WHERE username = '".$username."'";
									
							$query = $co->query($sql);
							
							$count = $query->rowCount();
							
							if($count==0)
							{
								echo '<p>This username doesn\'t exist.</p>';
							}
							else
							{
								// check if the user is already friend with this person
								$sql = "SELECT u.userID
										FROM Users u, Friends f
										WHERE f.userID = ".$_SESSION['userID']."
										AND f.friendID = u.userID
										AND u.username = '".$username."'";

								$query = $co->query($sql);
								
								$count = $query->rowCount();
								
								// if an account is found, error
								if($count>=1)
								{
									echo '<p>You are already friend with this user.</p>';
								}
								else
								{
									$
									
									$sql="SELECT userID FROM Users WHERE username = '".$username."'";
									
									$query = $co->query($sql);
									
									while($result = $query->fetch())
									{
										$insert = "INSERT INTO Friends values(".$_SESSION['userID'].", ".$result['userID'].")";
										$co->exec($insert);
									}
									
									echo '<p>'.$username.' has been added to you friend list.</p>';
								}
							}
							
						}
						
						$sql ="SELECT firstname, lastname
								FROM Users u, Friends f
								WHERE f.userID = ".$_SESSION['userID']."
								AND u.userID = f.friendID";
						$query = $co->query($sql);
						
						while($result = $query->fetch())
						{
							echo "<h5>".$result['firstname']." ".$result['lastname']."</h5>";
						}
						
						$query->closeCursor();
						
						?>
					</div>
				</div>
			</div>
			<script type="text/javascript" src="./lib/tau/mobile/js/tau.js"></script>
			<script src="./js/friends.js"></script> 
		 </div>
	</body>
</html>