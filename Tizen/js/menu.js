( function () {
	
   window.addEventListener( 'tizenhwkey', function( ev ) {
        if( ev.keyName === "back" ) {
            var activePopup = document.querySelector( '.ui-popup-active' ),
                page = document.getElementsByClassName( 'ui-page-active' )[0],
                pageid = page ? page.id : "";

            if( pageid === "main" && !activePopup ) {
                try {
                    tizen.application.getCurrentApplication().exit();
                } catch (ignore) {
                }
            } else {
                window.history.back();
                tau.back();
            }
        }
    } );
   
	document.querySelector('#movetoSchedule').onclick=function()
	{
		tau.changePage("schedule.html");
	};
	
    document.querySelector('#movetoFriends').onclick=function()
    {
   	 tau.changePage("friendslist.html");
    };
    
    document.querySelector('#movetoGroups').onclick=function()
    {
   	 tau.changePage("group.html");
    };
    
    document.querySelector('#movetoAppointment').onclick=function()
    {
   	 tau.changePage("new_appointment.html");
    };
    
    document.querySelector('#movetoDeadline').onclick=function()
    {
   	 tau.changePage("new_deadline.html");
    };
    
    document.querySelector('#movetoManageAppointment').onclick=function()
    {
   	 tau.changePage("change_appointment.html");
    };
    
    document.querySelector('#Disconnection').onclick=function()
    {
    	var IP="http://192.168.0.5:80";
    	var xhr = new XMLHttpRequest();
  		 
  		 xhr.onreadystatechange = function() {
  	    	 if (this.status === 200) {
  	    		tau.changePage("index.html");
  	    	 }
  	     };
  	     xhr.open("GET", IP + "/Web/disconnect.php", true);
  	     xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  	     xhr.send();
    };
    
} () );