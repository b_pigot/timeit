<!DOCTYPE html>
<html lang="en">
 	<head>
	    <meta charset="utf-8">  	
    	<meta name="viewport" content="width=device-width,user-scalable=no"/>
	   	<title>Menu</title>
	   	<link rel="stylesheet"  href="./lib/tau/mobile/theme/default/tau.css">
	   	<link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="./css/style.css"/> 
	</head>
  
 	<body>
 		<div class="ui-page" class="page">
		 	<div class="main">
				<h1>Sign up for free</h1>
				
				<form action="register.php" method="post">
					<input type="text" name="username" id="username" placeholder="Username"><br><br>
					<input type="password" name="psd" id="psd" placeholder="Password"><br><br>
					<input type="password" name="verify" id="verify" placeholder="Verify Password"><br><br>
					<input type="text" name="fname" id="fname" placeholder="First name"><br><br>
					<input type="text" name="lname" id="lname" placeholder="Last name"><br><br>
					<!--<input type="text" name="email" id="email" placeholder="Email"><br><br>-->
					<button type="submit" value="Submit" id="submit" name="confirm">Submit</button>
				</form>
				
				<?php
					session_start();

					if(isset($_POST['confirm']))
					{
						$username=htmlentities($_POST["username"]);
						$pwd=htmlentities($_POST["psd"]);
						$fname=htmlentities($_POST["fname"]);
						$lname=htmlentities($_POST["lname"]);
						
						require 'requires/connect.inc.php';
						
						// check if the username/password match an account in the database
						$sql = "SELECT userID FROM Users WHERE username = '".$username."'";
						$query = $co->query($sql);
						
						$count = $query->rowCount();
						
						// if an account is found, error
						if($count>=1)
						{
							echo '<p>Your username is already taken.</p>';
						}
						else
						{
							$sql = "INSERT INTO Users(username, password, firstname, lastname) values('".$username."', '".$pwd."', '".$fname."', '".$lname."')";
							
							$co->exec($sql);
							
							header("Location: indexb.php");
						}
					}

				?>
				
				<h4>Already a member? <a href="index.html">Login here</a></h4><br>
				<h4>Having some problems to register? please <a href="">Contact Us</a></h4>
			</div>
		</div>
		<script type="text/javascript" src="./lib/tau/mobile/js/tau.js"></script>
		<script src="./js/register.js"></script>
	</body>
	
</html>