DROP TABLE if exists Groups, Friends, Appointments, Reminders, Users;

CREATE TABLE Users (
userID int primary key AUTO_INCREMENT,
username varchar(25) not null,
password varchar(100) not null,
firstname varchar(25) not null,
lastname varchar(25) not null
);

INSERT INTO Users values
(1, 'djoko', 'tennis', 'Novak', 'Djokovic'),
(2, 'kidpoker', 'kid', 'Daniel', 'Negreanu'),
(3, 'hallyday', 'johnny', 'Jean-Philippe', 'Smet'),
(4, 'sparrow', 'jack', 'Johnny', 'Depp'),
(5, 'joker', 'bat', 'Heath', 'Ledger');

CREATE TABLE Reminders (
reminderID int primary key AUTO_INCREMENT,
reminderDate date not null,
reminderTime time not null
);

INSERT INTO Reminders values
(1, '2016-12-12', '11:00'),
(2, '2016-12-13', '16:35'),
(3, '2016-12-14', '23:10'),
(4, '2016-12-14', '23:45');

CREATE TABLE Appointments (
appID int AUTO_INCREMENT,
userID int,
weekday enum('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'),
appDate date,
appTime time not null,
reminderID int,
appNotes varchar(100),
primary key(appID, reminderID),
foreign key(userID) references User (userID) on delete cascade on update cascade,
foreign key(reminderID) references Reminder (reminderID) on delete cascade on update cascade
);

INSERT INTO Appointments values
(1, 1, 'Monday', '2016-12-12', '11:10', 1, ''),
(2, 1, 'Tuesday', '2016-12-13', '16:45', 2, ''),
(3, 3, 'Wednesday', '2016-12-14', '23:20', 3, ''),
(4, 5, 'Wednesday', '2016-12-14', '23:55', 4, '');

CREATE TABLE Friends (
userID int,
friendID int,
primary key(userID, friendID),
foreign key(userID) references User (userID) on delete cascade on update cascade,
foreign key(friendID) references User (userID) on delete cascade on update cascade
);

INSERT INTO Friends values
(2,5),
(1, 2),
(1, 3),
(1, 4);

CREATE TABLE Groups (
groupID int AUTO_INCREMENT,
userID int,
groupName varchar(50),
primary key(groupID, userID),
foreign key(userID) references User (userID) on delete cascade on update cascade
);

INSERT INTO Groups values
(1, 2, 'Software Engineering'),
(1, 3, 'Software Engineering'),
(1, 5, 'Software Engineering'),
(2, 1, 'Tennis'),
(3, 1, 'Gym'),
(4, 1, 'Coaching'),
(5, 1, 'Physics');