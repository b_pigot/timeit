var gulp = require('gulp'),
    sync = require('browser-sync').create();

gulp.task('default', function()
{
    sync.init({
        proxy: '/timeit',
        port: '80'
    })
});